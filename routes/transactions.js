var express = require('express');
var router = express.Router();

var mysql = require('mysql');
var settings = require('./settings.json');
var responsecode = require('./responesecode.json');

/* GET users listing. */
router.get('/history', function (req, res, next) {

    if (req.headers['x-api-key'] === settings.api_key) {

        var conn = mysql.createConnection({
            host: settings.db_host,
            user: settings.db_user,
            password: settings.db_pass,
            database: settings.db_name
        });

        conn.connect(function (err) {
            if (err) {
                res.statusCode = 500;
                res.json(responsecode.error_500);
            }
        });

        var user_id = req.query.user_id;

        if (user_id === null || user_id === '') {

            res.statusCode = 400;
            responsecode.error_400.message = 'Sorry, user_id is required';
            res.json(responsecode.error_400);

        } else {

            var query = "SELECT " +
                "hist.trans_id, " +
                "hist.description, " +
                "hist.nominal, " +
                "hist.created_at, " +
                "hist.from_user_id, " +
                "hist.to_user_id, " +
                "us1.name AS from_user_name, " +
                "us1.photo AS from_user_photo, " +
                "us2.name AS to_user_name, " +
                "us2.photo AS to_user_photo " +
                "FROM t_history AS hist " +
                "JOIN t_users AS us1 ON us1.user_id = hist.from_user_id " +
                "JOIN t_users AS us2 ON us2.user_id = hist.to_user_id " +
                "WHERE " +
                "hist.from_user_id = '" + user_id + "' " +
                "OR " +
                "hist.to_user_id = '" + user_id + "'";

            conn.query(query, function (err, result) {
                if (err) {
                    console.log(err);
                    res.statusCode = 500;
                    res.json(responsecode.error_500);
                } else {
                    if (result.length > 0) {

                        res.json(result);

                    } else {
                        res.statusCode = 400;
                        res.json(responsecode.error_400);
                    }
                }
            });
        }

    } else {
        res.statusCode = 401;
        res.json(responsecode.error_401);
    }

});

module.exports = router;
