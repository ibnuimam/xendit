var express = require('express');
var router = express.Router();

var mysql = require('mysql');
var settings = require('./settings.json');
var responsecode = require('./responesecode.json');
var crypto = require('crypto');

/* GET users listing. */
router.post('/login', function (req, res, next) {

    if (req.headers['x-api-key'] === settings.api_key) {

        var conn = mysql.createConnection({
            host: settings.db_host,
            user: settings.db_user,
            password: settings.db_pass,
            database: settings.db_name
        });

        conn.connect(function (err) {
            if (err) {
                res.statusCode = 500;
                res.json(responsecode.error_500);
            }
        });

        var email = req.body.email;
        var password = req.body.password;

        if (email === null || email === '' || password === null || password === '') {

            res.statusCode = 400;
            responsecode.error_400.message = 'Sorry, username and password is required';
            res.json(responsecode.error_400);

        } else {
            password = crypto.createHash('md5').update(password).digest("hex");
            var query = "SELECT * FROM t_users WHERE email = '" + email + "' AND password = '" + password + "'";
            conn.query(query, function (err, result) {
                if (err) {
                    console.log(err);
                    res.statusCode = 500;
                    res.json(responsecode.error_500);
                } else {
                    if (result.length > 0) {
                        res.json(result[0]);
                    } else {
                        res.statusCode = 400;
                        res.json(responsecode.error_400);
                    }
                }
            });
        }

    } else {
        res.statusCode = 401;
        res.json(responsecode.error_401);
    }

});

module.exports = router;
