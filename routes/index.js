var express = require('express');
var router = express.Router();

var Client = require('node-rest-client').Client;
var clientservice = require('./clientservice.json');

/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('index', {
        title: 'Xendit Test',
        user_id: '',
        result: {
            message: 'Please, type your user id for more information.'
        },
        status: 404
    });
});

router.post('/', function (req, res, next) {

    var user_id = req.body.user_id;
    var client = new Client();

    var url = clientservice.host + "/users/" + user_id;
    var args = {
        headers: {"X-API-KEY": clientservice.api_key}
    };

    client.get(url, args, function (data, response) {

        res.render('index', {
            title: 'Xendit Test',
            user_id: user_id,
            result: data,
            status: response.statusCode
        });

    });

});

module.exports = router;
